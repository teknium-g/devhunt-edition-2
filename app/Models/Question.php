<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 *
 * @property int $question_id
 * @property string|null $question_title
 * @property string|null $question_content
 * @property Carbon|null $question_date
 * @property int $user_id
 * @property int $tag_id
 *
 * @property User $user
 * @property Tag $tag
 * @property Collection|Answer[] $answers
 * @property Collection|Image[] $images
 *
 * @package App\Models
 */
class Question extends Model
{
	protected $table = 'questions';
	protected $primaryKey = 'id';
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int',
		'tag_id' => 'int'
	];

	protected $dates = [
		'question_date'
	];

	protected $fillable = [
		'question_title',
		'question_content',
		'question_date',
		'user_id',
		'tag_id'
	];

	public function user()
	{
		return $this->belongsTo(User::class, "user_id");
	}

	public function tag()
	{
		return $this->belongsTo(Tag::class, "tag_id");
	}

	public function answers()
	{
		return $this->hasMany(Answer::class);
	}

	public function images()
	{
		return $this->hasMany(Image::class);
	}
}
