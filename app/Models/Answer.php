<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Answer
 *
 * @property int $id
 * @property string|null $answer_content
 * @property Carbon|null $publish_date
 * @property int $question_id
 * @property bool|null $answer_valid
 * @property int $user_id
 *
 * @property Question $question
 * @property User $user
 * @property Collection|Comment[] $comments
 * @property Collection|Image[] $images
 * @property Collection|Rating[] $ratings
 *
 * @package App\Models
 */
class Answer extends Model
{
	protected $table = 'answers';
	protected $primaryKey = 'id';
	public $timestamps = false;

	protected $casts = [
		'question_id' => 'int',
		'answer_valid' => 'bool',
		'user_id' => 'int'
	];

	protected $dates = [
		'publish_date'
	];

	protected $fillable = [
		'answer_content',
		'publish_date',
		'question_id',
		'answer_valid',
		'user_id'
	];

	public function question()
	{
		return $this->belongsTo(Question::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class, "user_id");
	}

	public function comments()
	{
		return $this->hasMany(Comment::class);
	}

	public function images()
	{
		return $this->hasMany(Image::class);
	}

	public function ratings()
	{
		return $this->hasMany(Rating::class);
	}
}
