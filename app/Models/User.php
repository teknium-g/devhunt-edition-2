<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Laravel\Sanctum\Contracts\HasApiTokens;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens as SanctumHasApiTokens;

/**
 * Class User
 *
 * @property int $user_id
 * @property string|null $user_firstname
 * @property string|null $user_lastname
 * @property string|null $user_email
 * @property string|null $user_password
 * @property Carbon|null $inscription_date
 * @property bool|null $user_status
 * @property string|null $user_profile
 * @property string|null $user_phone
 * @property string|null $user_verification
 * @property int $level_id
 * @property int $roles_id
 *
 * @property Level $level
 * @property Role $role
 * @property Collection|Answer[] $answers
 * @property Collection|Rating[] $ratings
 * @property Collection|Group[] $groups
 * @property Collection|Question[] $questions
 *
 * @package App\Models
 */
class User extends Authenticatable
{
    use SanctumHasApiTokens;
	protected $table = 'users';
	protected $primaryKey = 'user_id';
	public $timestamps = false;

	protected $casts = [
		'user_status' => 'bool',
		'level_id' => 'int',
		'roles_id' => 'int'
	];

	protected $dates = [
		'inscription_date'
	];

	protected $hidden = [
		'user_password'
	];

	protected $fillable = [
		'user_firstname',
		'user_lastname',
		'user_email',
		'user_password',
		'inscription_date',
		'user_status',
		'user_profile',
		'user_phone',
		'user_verification',
		'level_id',
		'roles_id'
	];

	public function level()
	{
		return $this->belongsTo(Level::class);
	}

	public function role()
	{
		return $this->belongsTo(Role::class, 'roles_id');
	}

	public function answers()
	{
		return $this->hasMany(Answer::class);
	}

	public function ratings()
	{
		return $this->hasMany(Rating::class);
	}

	public function groups()
	{
		return $this->belongsToMany(Group::class, 'user_groups')
					->withPivot('ug_id');
	}

	public function questions()
	{
		return $this->hasMany(Question::class);
	}
    public function comments()
	{
		return $this->hasMany(Comments::class);
	}
}
