<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Group
 * 
 * @property int $group_id
 * @property string|null $group_name
 * 
 * @property Collection|User[] $users
 *
 * @package App\Models
 */
class Group extends Model
{
	protected $table = 'groups';
	protected $primaryKey = 'group_id';
	public $timestamps = false;

	protected $fillable = [
		'group_name'
	];

	public function users()
	{
		return $this->belongsToMany(User::class, 'user_groups')
					->withPivot('ug_id');
	}
}
