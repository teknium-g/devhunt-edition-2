<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rating
 * 
 * @property int $rating_id
 * @property int $answer_id
 * @property int $user_id
 * 
 * @property Answer $answer
 * @property User $user
 *
 * @package App\Models
 */
class Rating extends Model
{
	protected $table = 'ratings';
	protected $primaryKey = 'rating_id';
	public $timestamps = false;

	protected $casts = [
		'answer_id' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'answer_id',
		'user_id'
	];

	public function answer()
	{
		return $this->belongsTo(Answer::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
