<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Comment
 *
 * @property int $comment_id
 * @property string|null $comment_content
 * @property int $answer_id
 * @property int $user_id
 *
 * @property Answer $answer
 *
 * @package App\Models
 */
class Comment extends Model
{
	protected $table = 'comments';
	protected $primaryKey = 'comment_id';
	public $timestamps = false;

	protected $casts = [
		'answer_id' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'comment_content',
		'answer_id',
        'user_id'
	];

	public function answer()
	{
		return $this->belongsTo(Answer::class);
	}
    public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
}
