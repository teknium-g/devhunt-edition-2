<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Level
 * 
 * @property int $level_id
 * @property string|null $level_name
 * 
 * @property Collection|User[] $users
 *
 * @package App\Models
 */
class Level extends Model
{
	protected $table = 'levels';
	protected $primaryKey = 'level_id';
	public $timestamps = false;

	protected $fillable = [
		'level_name'
	];

	public function users()
	{
		return $this->hasMany(User::class);
	}
}
