<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserGroup
 * 
 * @property int $ug_id
 * @property int $user_id
 * @property int $group_id
 * 
 * @property User $user
 * @property Group $group
 *
 * @package App\Models
 */
class UserGroup extends Model
{
	protected $table = 'user_groups';
	protected $primaryKey = 'ug_id';
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int',
		'group_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'group_id'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function group()
	{
		return $this->belongsTo(Group::class);
	}
}
