<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 *
 * @property int $image_id
 * @property string|null $image_url
 * @property int $question_id
 * @property int $answer_id
 *
 * @property Question $question
 * @property Answer $answer
 *
 * @package App\Models
 */
class Image extends Model
{
	protected $table = 'images';
	protected $primaryKey = 'image_id';
	public $timestamps = false;

	protected $casts = [
		'question_id' => 'int',
		'answer_id' => 'int'
	];

	protected $fillable = [
		'image_url',
		'question_id',
		'answer_id'
	];

	public function question()
	{
		return $this->belongsTo(Question::class);
	}

	public function answer()
	{
		return $this->belongsTo(Answer::class, "answer_id");
	}
}
