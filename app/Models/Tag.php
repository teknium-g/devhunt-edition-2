<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tag
 * 
 * @property int $tag_id
 * @property string|null $tag_name
 * 
 * @property Collection|Question[] $questions
 *
 * @package App\Models
 */
class Tag extends Model
{
	protected $table = 'tags';
	protected $primaryKey = 'tag_id';
	public $timestamps = false;

	protected $fillable = [
		'tag_name'
	];

	public function questions()
	{
		return $this->hasMany(Question::class);
	}
}
