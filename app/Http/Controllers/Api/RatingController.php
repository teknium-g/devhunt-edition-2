<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $rating = Rating::with('user')
            ->with('answer')
            ->get();
        return response()->json([
            'ratings'    => $rating,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // store Rating using $request only user_id, answer_id, star return json response rating=rating
        $rating = Rating::create($request->only(['user_id', 'answer_id', 'star']));
        return response()->json([
            'rating'    => $rating,
        ], 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $rating = Rating::with('user')
            ->with('answer')
            ->where('rating_id', $id)
            ->get();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $rating = Rating::find($id);
        $rating->update($request->only(['user_id', 'answer_id', 'star']));
        return response()->json([
            'rating'    => $rating,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $rating = Rating::find($id);
        $rating->delete();
        return response()->json([
            'rating'    => $rating,
        ], 200);
    }
}
