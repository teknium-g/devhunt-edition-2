<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Image;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $answer = Answer::with('user')
            ->with('images')
            ->get();
        return response()->json([
            'questions'    => $answer,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
            $answer = new Answer();

            $answer->answer_content = $request->content;
            $answer->user_id = $request->user_id;
            $answer->question_id = $request->question_id;
            $answer->answer_valid = 0;
            $answer->created_at = date('Y-m-d H:i:s');
            $answer->save();

            $file = $request->file('images');

        if ($file) {
            $filename = $file->getClientOriginalName();
            $file->move(public_path('images'), $filename);

            $image = new Image();
            $image->image_url = "images/" . $filename;
            $image->answer_id = $answer->id;

            $image->save();
        }
            DB::commit();
            return response()->json([
                'success' => ' created',
            ], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function rating()
    {
        // $rate = DB::table('answers')->selectRaw('user_id, COUNT(answers.user_id) as count')->join('users', 'answers.user_id', 'users.user_id')->groupBy('answers.user_id')->orderBy('count','desc')->get();
        $rate = DB::table('answers')
            ->join('users', 'answers.user_id', 'users.user_id')
            ->select('users.user_id', 'users.user_profile', 'users.email', DB::raw('count(answers.id) as count'))
            ->groupBy('users.user_id')
            ->orderByDesc('count')
            ->limit(3)
            ->get();

        $rate2 = DB::table('answers')
            ->join('users', 'answers.user_id', 'users.user_id')
            ->select('users.user_id', 'users.user_profile', 'users.email', DB::raw('count(answers.id) as count'))
            ->groupBy('users.user_id')
            ->where('answer_valid', true)
            ->orderByDesc('count')
            ->limit(3)
            ->get();

        foreach ($rate as $key => $value) {
            $rate[$key]->rang= $key +1;
        }
        foreach ($rate2 as $key => $value) {
            $rate2[$key]->rang= $key +1;
        }

        return response()->json([
            'status' => 'success',
            'Many' => $rate,
            'Best' => $rate2
        ]);

    }

    public function validation( Request $request, $answer_id)
    {
        // return Answer::where('id', $answer_id)->first();
        try {
            $answer = Answer::where('id', $answer_id)->first();
                $answer->answer_valid = 1;
                $answer->update();

                return $answer;
        } catch (\Throwable $th) {
            //throw $th;
            return $th;
        }
    }
}
