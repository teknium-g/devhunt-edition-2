<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Tectalic\OpenAi\Authentication;
use Tectalic\OpenAi\Client;
use Tectalic\OpenAi\Manager;

class IAController extends Controller
{

    public function getAnswer(Request $request){

        $question = $request->question;
        $auth = new Authentication("Sre0fmhg19jhCrApLyYbT3BlbkFJRIiF1LgiBDG6iRgEq5kQ");
        $httpClient = new \GuzzleHttp\Client();
        $client = new Client($httpClient, $auth, Manager::BASE_URI);

        $response = $client->completions()->create(
            new \Tectalic\OpenAi\Models\Completions\CreateRequest([
                'model'  => 'text-davinci-002',
                'prompt' => "html is",
                'max_tokens' => 256,
            ])
        )->toModel();

        return $response->choices[0]->text;

    }
    public function gptResponse()
    {
        $openaiClient = \Tectalic\OpenAi\Manager::build(new \GuzzleHttp\Client(), new \Tectalic\OpenAi\Authentication('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'));

        /** @var \Tectalic\OpenAi\Models\ChatCompletions\CreateResponse $response */
        $response = $openaiClient->chatCompletions()->create(
            new \Tectalic\OpenAi\Models\ChatCompletions\CreateRequest([
                'model' => 'gpt-3.5-turbo',
                'messages' => [
                    ['role' => 'user', 'content' => 'Tell the world about the ChatGPT API in the style of a pirate'],
                ],
            ])
        )->toModel();

        return $model->choices[0]->message->content;
        // Ahoy there, me hearty! Gather round and listen well, for I'll be tellin' ye about the treasure trove known as ChatGPT API! ...
    }
}
