<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Ozekimessageout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OzekiMessageOutController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return DB::table('ozekimessageout')->get();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $db =  DB::table('ozekimessageout')->insert([
            'receiver' => $request->receiver,
            'msg' => $request->msg,
            'status' => 'send'
        ]);
        return response()->json([
            'msgStatus' => "succes",
        ], 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return DB::table('ozekimessageout')->where('id', $id)->get();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $db =  DB::table('ozekimessageout')->where('id', $id)->update([
            'receiver' => $request->receiver,
            'msg' => $request->msg,
            'status' => 'send'
        ]);
        return response()->json([
            'msgStatus' => "succes",
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Ozekimessageout::findOrFail($id)->delete();
    }
}
