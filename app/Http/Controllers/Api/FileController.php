<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class FileController extends Controller
{
    public function upload(Request $request)
    {
        try {
            // Augmenter le temps limite d'exécution à 300 secondes (5 minutes)
            set_time_limit(300);

            ini_set('memory_limit', '40M');

            $file = $request->file('file');
            $file->move(public_path('uploads'));

            return 'success';
        } catch (\Throwable $th) {
            return $th;
        }
    }

    //create an api to get all files in a directory


    public function getFiles()
    {
        $files = File::files(public_path('images'));
        $fileNames = array_map(function ($file) {
            //return  $file->getFilename();
            return ['file' => $file->getFilename()];
        }, $files);
        return $fileNames;
    }
    /**
     * Display a listing of the resource.
     */

    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if($request->hasFile('files')){
            $images = $request->file('files');
            foreach ($images as $image){
                $photoName  = $image->getClientOriginalName();

                $image->move(public_path('images/'), $photoName);
            }
            return response()->json([
                'message' => 'Fichier ajouter avec succès'
            ], 200);
        }
        else{
            return response()->json([
                'message' => 'Aucun fichier trouvé'
            ], 400);
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
