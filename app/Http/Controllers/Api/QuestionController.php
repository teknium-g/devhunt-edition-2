<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Http\Request;
use App\Models\Image;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $question = Question::with('user')
            ->with('tag')->with('images')
            ->with('answers.images', 'answers.user', 'answers.comments.user')
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json([
            'questions'    => $question,
        ], 200);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
         //File verification
         $files = $request->hasFile('images');

         DB::beginTransaction();
        try {
            $question = new Question();

            $question->question_title = $request->title;
            $question->question_content = $request->content;
            $question->user_id = $request->user_id;
            $question->tag_id = $request->tag_id;
            $question->created_at = date('Y-m-d H:i:s');
            $question->save();

            if($request->hasFile('images')){
                $images = $request->file('images');
                foreach ($images as $image){

                    // taking random_hexadecimals for 8 bytes
                    $setPhotoName  = bin2hex(random_bytes(8));

                    $photoExt = $image->getClientOriginalExtension();
                    $photoName = $setPhotoName . '.' . $photoExt;

                    $image->move(public_path('questions/'), $photoName);

                    $image = new Image();

                    $image->image_url = '/questions' . '/' . $photoName;
                    $image->question_id = $question->id;

                    $image->save();

                }
            }
            DB::commit();
            return response()->json([
                'success' => 'published',
            ], 201);

        }catch(\Throwable $th){
            DB::rollBack();
            return response()->json([
                'error' => 'create error',
                'message' => $th,
            ],400);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $question = Question::with('user')
            ->with('tag')->with('images')
            ->with('answers.images', 'answers.user', 'answers.comments.user')
            ->where('user_id', $id)
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json([
            'questions'    => $question,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $question = Question::find($id);
        $question->update($request->only(['question_title', 'question_content', 'user_id', 'tag_id']));
        return response()->json([
            'question'    => $question,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $question = Question::find($id);
        $question->delete();
        return response()->json([
            'question'    => $question,
        ], 200);
    }

    public function myPub($user_id)
    {
        $question = Question::with('user')
            ->with('tag')->with('images')->with('answers.images', 'answers.user')->where('user_id', $user_id)->orderBy('created_at', 'desc')
            ->get();
        return response()->json([
            'questions'    => $question,
        ], 200);
    }
    public function mostResponse($tag)
    {
        $question = Question::with('user')
            ->with('tag')->with('images')->with('answers.images', 'answers.user')->where('tag_id', $tag)->orderByRaw("COUNT(answers.answer_id) desc")
            ->get();

            return $question;
    }

    public function search(Request $request)
    {
        $content = $request->content;
        $question = Question::with('user')
            ->with('tag')->with('images')->with('answers.images', 'answers.user')->whereRaw("question_content LIKE '%$content%'")->orderBy('created_at', 'desc')
            ->get();

                return $question;
    }
}
