<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use ZipArchive;

class DownloadController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    public function downloadFiles(Request $request)
    {
        // Get the file names from the request
        $fileNames = $request->input('files');
        $responseString = trim($fileNames, "[]"); // remove square brackets from the string
        $responseArray = explode(',', $responseString); // split the string into an array using the comma delimiter
        $responseArray = array_map('trim', $responseArray); // remove any whitespace from each array element
        $cleanCodeName = array_map(function ($name) {
            return trim($name, '"');
        }, $responseArray);
        //dd($cleanCodeName);
        // Create a temporary zip file
        $zipFile = tempnam(sys_get_temp_dir(), 'download-');
        $zip = new ZipArchive();
        //dd($zipFile);
        $zip->open($zipFile, ZipArchive::CREATE);

        // Add each file to the zip file
        foreach ($cleanCodeName as $fileName) {
            $filePath = public_path('/images/' . $fileName);
            //dd($fileName);
            if (!file_exists($filePath)) {
                continue;
            }

            $zip->addFile($filePath, $fileName);
        }


        $zip->close();

        // Set the headers and download the zip file
        $headers = [
            'Content-Type' => 'application/zip',
        ];

        return response()->download($zipFile, 'files.zip', $headers);
    }
}
