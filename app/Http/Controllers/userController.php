<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class userController extends Controller
{
    public function register(Request $request)
    {

        if(User::where('email', $request->email)->first() != null){
            return response()->json([
                'status' => "error",
                "message" => "existing email"
            ], 400);
        }
        $verification = $request->file('verification');
        $profile = $request->file('profile');

        if ($profile) {
            $name = $request->phone;
            $ext = $profile->getClientOriginalExtension();
            $filename = $name . "." . $ext;
            $profile->move(public_path('profiles'), $filename);

            $image_url = "/profiles" . "/" . $filename;
        } else {
            $image_url = '/profiles/unknown.png';
        }
        if ($verification) {
            $filename = $verification->getClientOriginalName();
            $verification->move(public_path('verifications'), $filename);

            $image_url2 = "verifications/" . $filename;
        } else {
            return "OCR error ";
        }
        try {
            $user = new User();
            $user->user_firstname = $request->firstname;
            $user->user_lastname = $request->lastname;
            // $user->user_email = $validatedData['email'];
            $user->email = $request->email;
            $user->user_verification = $image_url2;
            $user->user_phone = $request->phone;
            $user->user_profile = $image_url;
            $user->level_id = $request->level_id;
            $user->level_id = 2;
            $user->roles_id = 1;
            $user->inscription_date =  date('Y-m-d H:i:s');

            $user->user_password = Hash::make($request->password);

            $user->save();

            $token = $user->createToken('token')->plainTextToken;

            return response()->json([
                'access_token' => $token,
                'token_type' => 'Bearer',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'status' => "error",
                "message" => 'error in subscription :' . $th
            ], 400);
        }

    }
    public function login(Request $request)
    {
        $user =  User::where('email', $request->email)->first();

        if (Hash::check($request->password, $user->user_password)) {
            auth()->guard('api')->setUser($user);
            $user->user_status = 1;
            $user->update();

            $token = $user->createToken($user->user_password)->plainTextToken;

            return response()->json([
                'status' => "succes",
                'token' => $token,
                'user' => $user
            ], 200);
        } else {
            return response()->json([
                'status' => "Erreur",
                'message' => "Mot de passe incorect"
            ], 401);
        }
    }
    public function logout($id)
    {
        $user = User::find($id)->update(["user_status" => 0]);

        return response()->json([
            'message' => 'user deconected',
        ], 200);
    }

    // public function login1(Request $request)
    // {
    //     $credentials = $request->only('email', 'password');

    //     // $credentials = User::where('email', 'teknium@gmail.com')->get();
    //     return $credentials;

    //     try {
    //         if (! $token = JWTAuth::attempt($credentials)) {
    //             return response()->json(['error' => 'invalid_credentials'], 401);
    //         }
    //     } catch (JWTException $e) {
    //         return response()->json(['error' => 'could_not_create_token'], 500);
    //     }

    //     return response()->json(compact('token'));
    // }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return User::all();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
