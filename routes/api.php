<?php

use App\Http\Controllers\Api\DownloadController;
use App\Http\Controllers\Api\FileController;
use App\Http\Controllers\roleController;
use App\Http\Controllers\userController;
use App\Http\Controllers\Api\GroupController;
use App\Http\Controllers\Api\LevelController;
use App\Http\Controllers\Api\QuestionController;
use App\Http\Controllers\Api\RatingController;
use App\Http\Controllers\Api\TagController;
use App\Http\Controllers\Api\AnswerController;
use App\Http\Controllers\Api\CommentController;
use App\Http\Controllers\Api\IAController;
use App\Http\Controllers\Api\OzekiMessageOutController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::resource('/groups', GroupController::class);
Route::resource('/tags', TagController::class);
Route::resource('/levels', LevelController::class);
Route::resource('/questions', QuestionController::class);
Route::resource('/comments', CommentController::class);

Route::get('/logout/{id}', [userController::class, 'logout']);
Route::post('/register', [userController::class, 'register']);
Route::post('/login', [userController::class, 'login']);
Route::get('/users', [userController::class, 'index']);
Route::get('/question_specs/{user_id}', [QuestionController::class, 'myPub']);
Route::get('/question/tag/{tag}', [QuestionController::class, 'mostResponse']);
Route::get('/result', [AnswerController::class, 'rating']);
Route::post('/search', [QuestionController::class, 'search']);


Route::resource('/levels', levelController::class);
Route::resource('/roles', roleController::class);
Route::resource('/ratings', RatingController::class);

Route::post('/chatresponse', [IAController::class, 'getAnswer']);
Route::post('upload', [FileController::class, 'upload']);

Route::resource('/ozekimessageouts', OzekiMessageOutController::class);
Route::resource('/answers', AnswerController::class);
Route::post('/download', [DownloadController::class, 'downloadFiles']);

Route::get('/files', [FileController::class, 'getFiles']);
Route::post('/lesson', [FileController::class, 'store']);
Route::post('/valide/{answer_id}', [AnswerController::class, 'validation']);
