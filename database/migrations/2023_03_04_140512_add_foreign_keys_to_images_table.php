<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->foreign(['question_id'], 'fkimages907425')->references(['question_id'])->on('questions');
            $table->foreign(['answer_id'], 'fkimages408623')->references(['answer_id'])->on('answers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->dropForeign('fkimages907425');
            $table->dropForeign('fkimages408623');
        });
    }
};
