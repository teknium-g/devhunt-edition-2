<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('answers', function (Blueprint $table) {
            $table->foreign(['question_id'], 'fkanswers55187')->references(['question_id'])->on('questions');
            $table->foreign(['user_id'], 'fkanswers87019')->references(['user_id'])->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answers', function (Blueprint $table) {
            $table->dropForeign('fkanswers55187');
            $table->dropForeign('fkanswers87019');
        });
    }
};
