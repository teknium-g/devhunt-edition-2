<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->foreign(['user_id'], 'fkquestions923961')->references(['user_id'])->on('users');
            $table->foreign(['tag_id'], 'fkquestions803287')->references(['tag_id'])->on('tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->dropForeign('fkquestions923961');
            $table->dropForeign('fkquestions803287');
        });
    }
};
