<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_groups', function (Blueprint $table) {
            $table->foreign(['user_id'], 'fkuser_group197176')->references(['user_id'])->on('users');
            $table->foreign(['group_id'], 'fkuser_group247422')->references(['group_id'])->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_groups', function (Blueprint $table) {
            $table->dropForeign('fkuser_group197176');
            $table->dropForeign('fkuser_group247422');
        });
    }
};
