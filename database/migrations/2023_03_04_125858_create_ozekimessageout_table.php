<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ozekimessageout', function (Blueprint $table) {
            $table->id();
            $table->string('sender')->nullable();
            $table->string('receiver');
            $table->string('msg');
            $table->string('senttime')->nullable();
            $table->string('receivedtime')->nullable();
            $table->string('status')->nullable();
            $table->string('operator')->nullable();
            $table->string('msgtype')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ozekimessageout');
    }
};
