<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('user_firstname')->nullable();
            $table->string('user_lastname')->nullable();
            $table->string('user_email')->nullable();
            $table->string('user_password')->nullable();
            $table->date('inscription_date')->nullable();
            $table->boolean('user_status')->nullable();
            $table->string('user_profile')->nullable();
            $table->string('user_phone', 12)->nullable();
            $table->string('user_verification')->nullable();
            $table->integer('level_id');
            $table->integer('roles_id');
            $table->timestamps();


            $table->unique(['user_id'], 'users_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
