<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('answer_id');
            $table->string('answer_content')->nullable();
            $table->date('publish_date')->nullable();
            $table->integer('question_id');
            $table->boolean('answer_valid')->nullable();
            $table->integer('user_id');
            $table->timestamps();

            $table->unique(['answer_id'], 'answers_answer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
    }
};
